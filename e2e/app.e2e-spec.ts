import { Autho0Page } from './app.po';

describe('autho0 App', () => {
  let page: Autho0Page;

  beforeEach(() => {
    page = new Autho0Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
